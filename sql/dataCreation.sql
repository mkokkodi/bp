
create table allSongsWSDM as select * from 
allSongsReleasedBetweenMarch08AndMarch23

#### Loading from crawlSongs.

LOAD DATA LOCAL INFILE '/Users/mkokkodi/Desktop/bigdata/beatport/raw/releases/afterMarch23/beforeCrash/songsReleases2.csv' 
INTO TABLE beatport.allSongsWSDM  FIELDS TERMINATED BY ',' ENCLOSED BY '' LINES TERMINATED BY '\n' IGNORE 1 LINES
(title,subtitle,trackid,label,genre,@released)
SET released = STR_TO_DATE(@released, '%Y-%m-%d');

create index songInd on allSongsWSDM(trackId);
create index dateInd on allSongsWSDM(released);

select * from allSongsWSDM;



LOAD DATA LOCAL INFILE '/Users/mkokkodi/Desktop/bigdata/beatport/raw/releases/afterMarch23/beforeCrash/artistsReleases2.csv' 
INTO TABLE beatport.artists  FIELDS TERMINATED BY ',' ENCLOSED BY '' LINES TERMINATED BY '\n' IGNORE 1 LINES
(artistId,artist,@crawledDate)
SET crawledDate = STR_TO_DATE(@crawledDate, '%Y-%m-%d');


LOAD DATA LOCAL INFILE '/Users/mkokkodi/Desktop/bigdata/beatport/raw/releases/afterMarch23/beforeCrash/remixerSongReleases2.csv' 
INTO TABLE beatport.remixerSong  FIELDS TERMINATED BY ',' ENCLOSED BY '' LINES TERMINATED BY '\n' IGNORE 1 LINES


LOAD DATA LOCAL INFILE '/Users/mkokkodi/Desktop/bigdata/beatport/raw/releases/afterMarch23/artistSongReleases2.csv' 
INTO TABLE beatport.artistSong  FIELDS TERMINATED BY ',' ENCLOSED BY '' LINES TERMINATED BY '\n' IGNORE 1 LINES
############################
############


# select artists to crawl --- for artist Info.
drop table if exists tmpArtistsToCrawl;
create table tmpArtistsToCrawl2 as
select count(distinct artistId) from allSongsWSDM a inner join artistSong atos 
on a.trackId = atos.trackId 
where artistId not in 
(select distinct artistId from artistInfo);




#Collect data for all artists and their labels
select distinct  Replace(artist,' ','%20') as artistClean,s.artistId from tmpArtistsToCrawl s inner join 
artists a on a.artistId = s.artistId;

#get the followers of these artists
select  distinct artist,a.artistId from tmpArtistsToCrawl t inner join artists a
on t.artistId = a.artistId  where a.artistId not in 
(select artistId from twitter);

# LOAD artists into artist Info:
LOAD DATA LOCAL INFILE '/Users/mkokkodi/Desktop/bigdata/beatport/raw/survival/artistsInfo.csv' 
INTO TABLE beatport.artistInfo  FIELDS TERMINATED BY ',' ENCLOSED BY '' LINES TERMINATED BY '\n' IGNORE 1 LINES;


LOAD DATA LOCAL INFILE '/Users/mkokkodi/Desktop/bigdata/beatport/raw/survival/songs.csv' 
INTO TABLE beatport.survivalSongs  FIELDS TERMINATED BY ',' ENCLOSED BY '' LINES TERMINATED BY '\n' IGNORE 1 LINES
(title,subtitle,trackId,label,genre,@released,labelId)
SET released = STR_TO_DATE(@released, '%Y-%m-%d');


#Go for labels

create index labelInd on survivalSongs(labelId);

create index labelInd on survivalLabelTracks(labelId);

select distinct label,labelId from survivalSongs s where
s.labelId not in (select distinct labelId from survivalLabelTracks);




LOAD DATA LOCAL INFILE '/Users/mkokkodi/Desktop/bigdata/beatport/raw/survival/labelReleases2.csv' 
INTO TABLE beatport.survivalLabelTracks3 FIELDS TERMINATED BY ',' ENCLOSED BY '' LINES TERMINATED BY '\n' IGNORE 1 LINES
(trackId,artistId,labelId,genre,@released,price)
SET released = STR_TO_DATE(@released, '%Y-%m-%d');

create table survivalLabelTracksComplete as
select * from survivalLabelTracks
union 
select * from survivalLabelTracks2
union 
select * from survivalLabelTracks3;

#drop index artistInd on survivalLabelTracksComplete;
create index labelInd on survivalLabelTracksComplete(labelId);
create index dateInd on survivalLabelTracksComplete(released);
create index artistInd on survivalLabelTracksComplete(artistId);
create index trackId on survivalLabelTracksComplete(trackId);


#create table w subset of artists -- the complete table is super heavy
drop table if exists survivalLabelTrackFollowers;
create table survivalLabelTrackFollowers as
select distinct s.*, t.followers from completeTwitter t inner join 
survivalLabelTracksComplete s on t.artistId = s.artistId
and trackId > 0;


create index labelInd on survivalLabelTrackFollowers(labelId);
create index dateInd on survivalLabelTrackFollowers(released);
create index artistInd on survivalLabelTrackFollowers(artistId);
create index trackId on survivalLabelTrackFollowers(trackId);


#Create entropy raw file "labelsEntropyRaw"
#Maybe think whethe I should do this per week. 
select distinct trackId,labelId,genre from survivalLabelTrackFollowers where 
released < '2015-03-08';


#How many songs are we analyzing?
select count(distinct trackId) from survivalLabelTrackFollowers where released between '2015-03-07' and '2015-05-11'
#Load entropies
delete from labelsEntropies;
LOAD DATA LOCAL INFILE '/Users/mkokkodi/Desktop/bigdata/beatport/data/labelsEntropies.csv' 
INTO TABLE beatport.labelsEntropies  FIELDS TERMINATED BY ',' ENCLOSED BY '' LINES TERMINATED BY '\n' IGNORE 1 LINES;

create index labelInd on labelsEntropies(labelId);

select * from labelsEntropies limit 10;

#Create Weekly label Trends:
#select week('2015-03-14')
drop table if exists labelTrend;
create table labelTrend as
select labelId,week,count(distinct trackId) as trend from
(
select labelId,s.trackId, TRUNCATE((Datediff(crawledDate,'2015-03-07'))/7,0) as week, Datediff(crawledDate,'2015-03-07') as days
from survivalLabelTrackFollowers s inner join ranks r on r.trackId = s.trackId
)r group by labelId,week;

## Olf label trend
#create table labelTrend as
#select labelId,count(distinct s.trackId) as trending from ranks r inner join survivalLabelTrackFollowers s
#on s.trackId = r.trackId 
#where released < '2015-03-23'
#group by labelId

create index labelInd on labelTrend(labelId);

create index weekInd on labelTrend(week);



## Create table that computes the death date
drop table if exists songsDeath;
create table songsDeath as
select r.trackId,min(DATEDIFF(crawledDate,released)) as 'days', min(TRUNCATE((Datediff(released,'2015-03-07'))/7,0)) as week from 
ranks r inner join songs s on s.trackId = r.trackId 
where released between '2015-03-07' and '2015-05-11'
group by r.trackId;



create index trackInd on songsDeath(trackId);
create index weekInd on songsDeath(week);

select * from songsDeath limit 10;

# Create artists Entropies table
# Raw file for entropy:  artistsEntropiesRaw.csv
select distinct trackId,artistId,genre from survivalLabelTrackFollowers where 
released < '2015-03-08';


delete from artistsEntropies;
LOAD DATA LOCAL INFILE '/Users/mkokkodi/Desktop/bigdata/beatport/data/artistsEntropies.csv' 
INTO TABLE beatport.artistsEntropies  FIELDS TERMINATED BY ',' ENCLOSED BY '' LINES TERMINATED BY '\n' IGNORE 1 LINES;

create index artistId on artistsEntropies(artistId);

select * from artistsEntropies limit 10;


### Create dataset for survival training
drop table if exists trainData;
create table trainData as
select distinct l.trackId,l.artistId,l.labelId, le.numberOfSongs as r_l, if(trend is null,0,trend) as t_l,
le.entropy, followers as fi, a.numberOfSongs as r_i, a.entropy as e_i, if(days is NULL,60,days) as days, l.genre #, if(days is NULL,0,1) as type # this is for right-censored
from  survivalLabelTrackFollowers l 
inner join labelsEntropies le on le.labelId = l.labelId 
inner join artistsEntropies a on a.artistId = l.artistId
inner join songsDeath sd on l.trackId = sd.trackId  # left join for right-censored
left outer join labelTrend lt on lt.labelId=l.labelId and lt.week = sd.week-1 #get the trend of previous week
#where l.trackId > 0 and trending is not Null and followers is not null
#and l.released between '2015-03-07' and '2015-03-23' #only if right censored
;

#same song multiple artists
select * from trainData t inner join 
(
select trackId from trainData
group by trackId
having count(*) > 1) r on r.trackId = t.trackId
order by t.trackId;

#Extrack dataset for R: survivalTrain.csv
select  trackId,labelId,r_l,t_l,entropy,max(r_i) as r_i,max(e_i) as e_i,days,genre, (max(fi)+1) as fi from trainData
group by trackId,labelId,r_l,t_l,entropy,days,genre;



# Create table for weeks for songs that didn't get on the charts
#maybe I should get the estimate of the previous week? 
drop table if exists songsAlive;
create table songsAlive as
select distinct r.*, TRUNCATE((Datediff(released,'2015-03-07'))/7,0) as week from 
survivalLabelTrackFollowers r left outer join songsDeath s on s.trackId = r.trackId 
where released between '2015-03-07' and '2015-05-11'
and days is null;

select * from songsAlive limit 10;

create index labelInd on songsAlive(labelId);
create index artistInd on songsAlive(artistId);
create index weekInd on songsAlive(week);

#Create test set
drop table if exists testData;
create table testData as
select distinct l.trackId,l.artistId,l.labelId, le.numberOfSongs as r_l, if(trend is null,0,trend) as t_l,
le.entropy, followers as fi, a.numberOfSongs as r_i, a.entropy as e_i,  l.genre #, if(days is NULL,0,1) as type # this is for right-censored
from  songsAlive l 
inner join labelsEntropies le on le.labelId = l.labelId 
inner join artistsEntropies a on a.artistId = l.artistId
left outer join labelTrend lt on lt.labelId=l.labelId and lt.week = l.week-1
;


#Extrack dataset for R: survivalTest.csv
select  trackId,labelId,r_l,t_l,entropy,max(r_i) as r_i,max(e_i) as e_i,genre, (max(fi)+1) as fi from testData
group by trackId,labelId,r_l,t_l,entropy,genre;


#Load probabilities (used of awk for cleaning them:  awk -F"," '{print $2,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21}' probabilities.csv > probabilitiesClean.csv 
LOAD DATA LOCAL INFILE '/Users/mkokkodi/Dropbox/projects/beatport/data/probabilitiesClean.csv' 
INTO TABLE beatport.survivalProbabilities  FIELDS TERMINATED BY ' ' ENCLOSED BY '' LINES TERMINATED BY '\n' IGNORE 1 LINES;

select * from beatport.survivalProbabilities;



#trainForTrees.csv file
drop table if exists trainForTrees;
create table trainForTrees as
select distinct t.trackId,  log(r_l) as r_l,log(t_l+1) as t_l,entropy, log(fi+1) as fi, log(r_i) as r_i, datediff(released,'2015-03-07') as daysAfterRelease,t.genre, log (e_i) as e_i, '1' as 'type'
from trainData t join survivalLabelTrackFollowers s on t.trackId = s.trackId
union 
select  distinct  d.trackId,  log(r_l) as r_l,log(t_l+1) as t_l,entropy, log(fi) as fi, log(r_i) as r_i, datediff(released,'2015-03-07') as daysAfterRelease,s.genre, log (e_i) as e_i, '0' as 'type'
 from testData
t join survivalLabelTrackFollowers s on s.trackId = t.trackId inner join survivalProbabilities d on t.trackId = d.trackId
where prob50 <= 0.005;

select * from trainForTrees where fi is null;
#Extrack dataset with uniq tracks
select  trackId,r_l,t_l,entropy,max(r_i) as r_i,max(e_i) as e_i, max(fi) as fi, daysAfterRelease,type,genre 
from trainForTrees
where fi is not null
group by trackId,r_l,t_l,entropy,daysAfterRelease,type,genre ;

#Estimate quantiles for treatment

select    r_l,t_l,entropy from trainForTrees


