

drop table if exists survivalSongs;
CREATE TABLE `survivalSongs` (
  `title` varchar(300) NOT NULL,
  `subtitle` varchar(300) DEFAULT NULL,
  `trackid` bigint(20) NOT NULL,
  `label` varchar(300) DEFAULT NULL,
  `genre` varchar(100) DEFAULT NULL,
  `released` datetime DEFAULT NULL,
  `labelId` bigint(20) NOT NULL,
  PRIMARY KEY (`trackid`)
) ENGINE=InnoDB DEFAULT CHARSET=big5;



drop table if exists survivalLabelTracks;
CREATE TABLE `survivalLabelTracks` (
  `trackId`  bigint(20) NOT NULL,
  `artistId`  bigint(20) NOT NULL,
  `labelId` bigint(20) NOT NULL,
  `genre` varchar(100) DEFAULT NULL,
  `released` datetime DEFAULT NULL,
  `price` decimal(10,3) NOT NULL,
  PRIMARY KEY (`trackid`,`artistId`)
) ENGINE=InnoDB DEFAULT CHARSET=big5;

drop table if exists survivalProbabilities;
CREATE TABLE `survivalProbabilities` (
  `trackId`  bigint(20) NOT NULL,
  `prob5` decimal(10,3) NOT NULL,
  `prob10` decimal(10,3) NOT NULL,
  `prob15` decimal(10,3) NOT NULL,
  `prob20` decimal(10,3) NOT NULL,
  `prob25` decimal(10,3) NOT NULL,
  `prob30` decimal(10,3) NOT NULL,
  `prob35` decimal(10,3) NOT NULL,
  `prob40` decimal(10,3) NOT NULL,
  `prob45` decimal(10,3) NOT NULL,
  `prob50` decimal(10,3) NOT NULL,
  PRIMARY KEY (`trackid`)
) ENGINE=InnoDB DEFAULT CHARSET=big5;

CREATE TABLE `twitterJuly12` (
  `handle` varchar(100) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `artist` varchar(150) DEFAULT NULL,
  `artistId` bigint(20) NOT NULL,
  `followers` bigint(20) DEFAULT NULL,
  `prob` decimal(10,3) DEFAULT NULL,
  PRIMARY KEY (`handle`,`artistId`),
  KEY `artistInd` (`artistId`)
) ENGINE=InnoDB DEFAULT CHARSET=big5;

CREATE TABLE `survivalLabelTracks3` (
  `trackId` bigint(20) NOT NULL,
  `artistId` bigint(20) NOT NULL,
  `labelId` bigint(20) NOT NULL,
  `genre` varchar(100) DEFAULT NULL,
  `released` datetime DEFAULT NULL,
  `price` decimal(10,3) NOT NULL,
  PRIMARY KEY (`trackId`,`artistId`),
  KEY `dateInd` (`released`),
  KEY `labelInd` (`labelId`)
) ENGINE=InnoDB DEFAULT CHARSET=big5;

drop table if exists artistInfo;
CREATE TABLE `artistInfo` (
  `artistId` bigint(20) NOT NULL,
    `songId` bigint(20) NOT NULL,
  `remixer` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=big5;



drop table if exists labelsEntropies;
CREATE TABLE `labelsEntropies` (
  `labelId` bigint(20) NOT NULL,
    `entropy` decimal(10,3) NOT NULL,
    `numberOfSongs` int Not Null
) ENGINE=InnoDB DEFAULT CHARSET=big5;