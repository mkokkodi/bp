


select distinct r.artist,r.artistId from artistsForTwitter a inner join 
artists r on r.artistId = a.artistId and r.artistId not in
(select artistId from twitter)

LOAD DATA LOCAL INFILE '/Users/mkokkodi/Desktop/bigdata/beatport/raw/labelLink.csv' 
INTO TABLE beatport.labelLink FIELDS TERMINATED BY ',' ENCLOSED BY '' LINES TERMINATED BY '\n' IGNORE 1 LINES;

LOAD DATA LOCAL INFILE '/Users/mkokkodi/Desktop/bigdata/beatport/raw/labelReleases.csv' 
INTO TABLE beatport.labelRelease  FIELDS TERMINATED BY ',' ENCLOSED BY '"' LINES TERMINATED BY '\n' IGNORE 1 LINES
(reviewId,reviewerId,asin,review,title,@dateWritten,helpfulVotes,prcHelpful,totalVotes,productRating,verified,badges)
SET dateWritten = STR_TO_DATE(@dateWritten, '%d %M %Y');

select min(crawledDate) from ranksGenre;

select trackId,rank,crawledDate,DATEDIFF(crawledDate,'2015-03-08') as 'days',genre from ranksGenre
where genre = 'trance'
limit 10000

select * from labelRelease limit 10

create table artistLabel as
select t.artistId, followers,label from labelRelease l inner join 
twitter t on t.artistId = l.artistId


select min(crawledDate) from ranks order by crawledDate


create index labInd on artistLabel(label)

create index aInd on artistLabel(artistId)

select artistId,replace(label,'&#39;','') as labelClean,count(*) as colabs, followers
from artistLabel group by artistId,label
having count(*) > 5
limit 100






drop table if exists allSongsWSDM;
CREATE TABLE `allSongsWSDM` (
  `title` varchar(300) NOT NULL,
  `subtitle` varchar(300) DEFAULT NULL,
  `trackid` bigint(20) NOT NULL,
  `label` varchar(300) DEFAULT NULL,
  `genre` varchar(100) DEFAULT NULL,
  `released` datetime DEFAULT NULL
  ,
  PRIMARY KEY (`trackid`)
) ENGINE=InnoDB DEFAULT CHARSET=big5;



## Create dataset with all released songs between 
# March 15 (i.e. 7 days after starting collecting(
#and May 11 (last day of collection)
drop table if exists survivalLabelTrackFollowersWSDM;
create table survivalLabelTrackFollowersWSDM as
select * from survivalLabelTrackFollowers
where released > '2015-03-15' 

select count(distinct t.trackId) from tracksWsdm t inner join ranks r
on t.trackId = r.trackId


select distinct artistId,songId,genre 
from artistInfo a inner join survivalSongs s on s.trackId = a.songId
where released < '2015-03-23'
 limit 10

drop table if exists labelNumberOfArtists;
create table labelNumberOfArtists as
select labelId, count(distinct artistId) as numberOfartists
from survivalLabelTracks 
group by labelId

create index labelId on labelNumberOfArtists(labelId);
create index artistInd on artistsEntropies(artistId);

select * from allVerifiedTwitter
#Create labels trends:


create index artistInd on artistInfo(artistId);

select * from artistInfo limit 10

select count(distinct sd.trackId) 
from songsDeath sd 
inner join survivalLabelTracks l on l.trackId = sd.trackId  
inner join labelsEntropies le on le.labelId = l.labelId 
inner join labelNumberOfArtists ln on ln.labelId = l.labelId 
#inner join artistInfo ai on ai.songId = sd.trackId 
#inner join allVerifiedTwitter tw on tw.artistId = ai.artistId 
inner join artistsEntropies a on a.artistId = ai.artistId
left outer join  labelTrend lt on lt.labelId=l.labelId

select * from trainData




drop table if exists survivalData;
create table  survivalData as
select distinct s.artistId,genre,r.trackId,label 
from ranks r inner join artistSong s on r.trackId = s.trackId
inner join songs so on so.trackId = s.trackId inner join 
allVerifiedTwitter a on a.artistId = s.artistId
where released between '2015-03-07' and '2015-05-12'
#select labels to crawl (labelsSurvival.csv)
select distinct label,labelId from survivalSongs where released between '2015-03-07' and 
'2015-03-23'
limit 10;

select distinct(crawledDate) from ranks
order by crawledDate desc;


 order by followers desc