




from sets import Set
import mechanize
import random
import time

path = "/Users/mkokkodi/Desktop/bigdata/beatport/raw/"


baseLink = "https://pro.beatport.com/"

artists = {}
artistsLinks = {}
remixers = {}
#remixersLink = Set()
title=-1
genre = -1
released = -1
price = -1
trackId = -1
rank = -1
label = -1
labelId = -1
subtitle = -1
labelId=-1
def getBrowser():
    br = mechanize.Browser()
    br.addheaders = [('user-agent', '   Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.2.3) Gecko/20100423 Ubuntu/10.04 (lucid) Firefox/3.6.3'), ('accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')]
    ##Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36


    # Don't handle HTTP-EQUIV headers (HTTP headers embedded in HTML).
    br.set_handle_equiv(False)

    # Ignore robots.txt.  Do not do this without thought and consideration.
    br.set_handle_robots(True)

    # Don't add Referer (sic) header
    br.set_handle_referer(False)

    # Don't handle Refresh redirections
    br.set_handle_refresh(False)
    
    return br
    
    
    
def getHTML(br,url):
    page=br.open(url)
    html=page.read() 
    return html



def crawl(htmlPage,outPage):
    try:
        print "crawling now "+htmlPage
        br = getBrowser()
        html = getHTML(br, htmlPage)
        fout = open("tmp/"+outPage,"w")
        for line in html:
            fout.write(line)
        print "done"
        fout.close()
        return True
    except:
        print "Crawling failed.."
        return False



def initialize():
    global rank,artists,remixers,price, title, genre,released,price,trackId, rank, label, labelId, subtitle
    
   
    artists = {}
    
    remixers = {}
    #remixersLink = Set()
    title=-1
    genre = -1
    released = -1
    price = -1
    trackId = -1
    
    label = -1
    labelId = -1
    subtitle = -1
    price = -1
    rank = -1
    labelId = -1

def parseTopPage(fin,link,getNextLinks):
    
    global rout,fout,newRemixer,rank,artists,remixers,price, title, genre,released,price,trackId, rank, label, labelId, subtitle
 
    newRemixer = False
    newTrack = False
    
    if(getNextLinks):
        nextLinks=Set()
    else:
        nextLinks = None
    print link
    for line in fin.readlines():
        
        #print "<a href=\"/"+link+"/releases?page="
        if getNextLinks and "class=\"pag-number\">" in line:
            #print line
            nextLinks.add(line.split("class=\"pag-number\">")[1].replace("</a>","").replace("\n",""))
        
        if 'bucket-item track' in line:
            newTrack = True
           
            #print title
            
        if "buk-track-primary-title" in line:
            title = line.split("buk-track-primary-title\">")[1]
            title = title.replace("</span>","").replace("\n","")
        
        if newTrack and 'buk-track-remixed' in line:
            subtitle = line.split("buk-track-remixed\">")[1]
            subtitle = subtitle.replace("</span>","").replace("\n","")
        if newTrack and '<p class="buk-track-artists">' in line:
            newArtist  = True
        if newTrack and  '<p class="buk-track-remixers">' in line:
            newRemixer  = True
            #     if newTrack and 'data-track=' in line:
            #print line
            #     trackId = line.split("data-track=")[1].split(">")[0].replace("\"","")
            #print trackId
        if newTrack and 'data-label=' in line:
            #labelId = line.split(" data-label=")[1].split(">")[0].replace("\"","")
            label = line.split(">")[1].split("<")[0]
            labelId = line.split("data-label=")[1]
            labelId = labelId.split(">")[0].replace("\"","")

            #print line
        if  newTrack and "<div class=\"buy-button track-buy-button\" data-track=\""  in line:
            trackId = line.split("data-track=\"")[1].split("\"")[0]
            price  = line.split("data-price=\"")[1].split("\">")[0].replace("$","")
       
       
        if newTrack and 'data-artist=' in line and newArtist and not newRemixer:
            djId = line.split("data-artist=")[1].split(">")[0].replace("\"","")
            
            name = line.split(">")[1].split("<")[0]
            #print djId,name
            artists[name]=djId
            #newArtist = False
            #print line
            #print name
        if newTrack and 'data-artist=' in line and newRemixer:
            djId = line.split("data-artist=")[1].split(">")[0].replace("\"","")
            name = (line.split(">")[1].split("<")[0])
            remixers[name]=djId
            #print "----->", line
            
            #print line.split("href=\"")[1].split("\"")[0]
            #newRemixer = False
        if newTrack and 'data-genre=' in line:
            genre = line.split(">")[1].split("<")[0]
        if newTrack and '<p class="buk-track-released"' in line:
            released = line.split(">")[1].split("<")[0]
        if newTrack and '</li>' in line:
            #print "Writing Files..."
            #price  = line.split(">")[1].split("<")[0].replace("$","")
            newTrack = False
            newArtist = False
            newRemixer = False
            fout.write(str(title)+","+str(subtitle)+","+str(trackId) +","+str(label)+","+str(genre)+","+str(released)+","+str(labelId)+"\n")
            
            for artist,artistId in artists.iteritems():
                rout.write(str(artistId)+","+str(trackId)+",False\n")

            for remixer, remixerId in remixers.iteritems():
                rout.write(str(remixerId)+","+str(trackId)+",True\n")
                    
            #print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
            initialize()
            #print len(artists)
    if nextLinks is not None:        
        return nextLinks 

rout = open(path+"survival/artistsInfo.csv","w")
rout.write("artist,song,remixer\n")
fout = open(path+"survival/songs.csv","w")
fout.write("title,subtitle,trackId,label,genre,released,labelId\n")  
  
fin = open("/Users/mkokkodi/Dropbox/projects/beatport/data/artistsToCrawl.csv")
#artistClean,artistIdq
artistId = {}
for line in fin.readlines():
    artistId[line.split(",")[0]] = line.split(",")[1].replace("\n","")

#artistId["12th%20Planet"]=36706
#artistId['Above%20&amp;%20Beyond']=7181
#https://pro.beatport.com/artist/Gabrielle%20Ross/377673/tracks
for artist,id in artistId.iteritems():
    
    nextLinks = None
    link = baseLink+"artist/"+artist+"/"+str(id)+"/tracks"
    if crawl(link,"artistTracks.html"):
        
        #print "Crawling ",link
        waitingTime = random.randint(1, 2)
        print "Waiting for seconds:",waitingTime
        time.sleep(waitingTime)
    fin = open("tmp/artistTracks.html")
    
    
    nextLinks = parseTopPage(fin,link,True)
    print nextLinks
    if nextLinks is not None:
        for link2 in nextLinks:
            if crawl(link+"?page="+str(link2),"artistTracks.html"):
    
                waitingTime = random.randint(1, 2)
                print "Waiting for seconds:",waitingTime
                time.sleep(waitingTime)
    
                fin = open("tmp/artistTracks.html")
                parseTopPage(fin,link,False)
        #break
    
rout.close()
fout.close()
