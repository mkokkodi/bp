

from sets import Set
path = "/Users/mkokkodi/Dropbox/projects/beatport/data/"

fin = open(path+"daysAfterAnalysis.csv")

groupByGenre={}
#trackId,days,daysAfterRelease,genre
for line in fin.readlines():
    if "trackId,days," not in line:
        tmpAr  =line.replace("\n","").split(",")
        genre = tmpAr[-1]
        days = tmpAr[1]
        daysAfterRelease = tmpAr[2]
        trackId = tmpAr[0]
        if genre not in groupByGenre:
            groupByGenre[genre]={}
        if days not in  groupByGenre[genre]:
            groupByGenre[genre][days]={}
        groupByGenre[genre][days][trackId] = daysAfterRelease

fin.close()
songsInFirstMonthPerGenre = {}
for genre, daysTrackDaysAfter in groupByGenre.iteritems():
    if genre not in songsInFirstMonthPerGenre:
        songsInFirstMonthPerGenre[genre] = Set()
    for days,trackDaysAfter in daysTrackDaysAfter.iteritems():
        if int(days) < 30:
            for track,daysAfter in trackDaysAfter.iteritems():
                songsInFirstMonthPerGenre[genre].add(track)
                



genreFirstInstancesAlreadySeen = {}
resultsByGenre = {}
for genre, daysTrackDaysAfter in groupByGenre.iteritems():
    if genre not in genreFirstInstancesAlreadySeen:
        genreFirstInstancesAlreadySeen[genre] = Set()
        resultsByGenre[genre] = []
    for days,trackDaysAfter in daysTrackDaysAfter.iteritems():
        if int(days) >= 30:
            for track,daysAfter in trackDaysAfter.iteritems():
                if(track not in songsInFirstMonthPerGenre[genre]):
                    if track not in genreFirstInstancesAlreadySeen:
                        genreFirstInstancesAlreadySeen[genre].add(track)
                        resultsByGenre[genre].append(daysAfter)

fout = open(path+"resultsDaysAfter.csv","w")
fout.write("genre,days\n")
for genre,line in resultsByGenre.iteritems():
    for row in line:
        fout.write(genre.replace("\"","")+","+str(row)+".0"+"\n")
        
fout.close()
           
        