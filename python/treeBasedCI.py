from sklearn import tree


import numpy as np
from pandas import DataFrame
import warnings
warnings.filterwarnings("ignore")

path = "/Users/mkokkodi/Dropbox/projects/beatport/data/"

def runTrees():
    newTrainDf = DataFrame.from_csv(path+"cleanTraining.csv", header=False, sep=',',index_col=False)
    independentVars=['fi','r_i','e_i','daysAfterRelease']
    trainX = newTrainDf[independentVars]
    trainY = newTrainDf['label']
    trainX = trainX.astype('float32')    
    for var in independentVars:
        print var, trainX[var].mean(),trainX[var].max()
 
    clf = tree.DecisionTreeClassifier(min_samples_split=300)
    clf = clf.fit(trainX.values,trainY.values)
    tree.export_graphviz(clf, out_file="/Users/mkokkodi/Dropbox/projects/beatport/data/tree.dot")
    leaves = clf.tree_.apply(trainX.values)
    newTrainDf['leaf']=leaves
    header = ['trackId','type','leaf','label']
    newTrainDf.to_csv(path+"resultTreeFile.csv", sep=',', encoding='utf-8', index=False,cols=header)
       
    #print leaves
    #print len(leaves)



def calculateEffect():
    global numberOfInstancesThreshold,fout
    df = DataFrame.from_csv(path+"resultTreeFile.csv", header=False, sep=',',index_col=False)
    leaves =  np.unique(df['leaf'].values)
    print "unique leaves:",len(leaves)
    classes = np.unique(df['label'].values)
    results = {}
    leafSize = {}
    totalSize = len(df)
    baselineLabel = "0_0_0"
    for leaf in leaves:
        #print "New Leaf"
        results[leaf]={}
        leafDf = df[df['leaf']==leaf]
        
        #leafSize[leaf]=len(leafDf)
        sizes = []
        #print "Leaf Size:",len(leafDf)
        baselineDf = leafDf[leafDf['label']==baselineLabel]
        sizes.append(len(baselineDf))
        if(len(baselineDf) > numberOfInstancesThreshold):
            baselineEffect = float(baselineDf['type'].sum())/float(len(baselineDf))
            #print "baselineEffect",baselineEffect
            for c in classes:
                if c != baselineLabel:
                    classDf = leafDf[leafDf['label']==c]
                    if len(classDf) > numberOfInstancesThreshold:
                        sizes.append(len(classDf))
                        classEffect = float(classDf['type'].sum())/float(len(classDf))
                        TE = classEffect-baselineEffect
                        #print c, TE,float(classDf['type'].sum()),float(len(classDf)),float(baselineDf['type'].sum()),float(len(baselineDf))
                        results[leaf][c]=TE
        leafSize[leaf] = min(sizes)
    ATEperLabel = {}
    totalDenom = {}    
    for leaf,d in results.iteritems():
        #print "---- new leaf ---"
        for treatment,TE in d.iteritems():
            #print "%0.3f" %treatment,"%0.3f" %TE
            if treatment not in ATEperLabel:
                ATEperLabel[treatment]=0
                totalDenom[treatment] = 0
            ATEperLabel[treatment] += leafSize[leaf] * TE
            totalDenom[treatment] +=  leafSize[leaf]
    print "treatment \t ATE"
    for treatment,ate in ATEperLabel.iteritems():
        res = ate/float(totalDenom[treatment])
        print treatment,"\t %0.3f" %res
        #tmpAr = treatment.split("_")
        tmpAr = getLabel(treatment);
        fout.write(tmpAr[0]+","+tmpAr[1]+","+tmpAr[2]+","+tmpAr[0]+"_"+tmpAr[1]+","+str(res)+",tree\n")
        
            
         
     
          
def runRandom():
    global fout
    df = DataFrame.from_csv(path+"cleanTraining.csv", header=False, sep=',',index_col=False)
    classes = np.unique(df['label'].values)
    baselineLabel = "0_0_0"
    baselineDf = df[df['label']==baselineLabel]
    baselineEffect = float(baselineDf['type'].sum())/float(len(baselineDf))
    for c in classes:
        if c != baselineLabel:
            classDf = df[df['label']==c]
            classEffect = float(classDf['type'].sum())/float(len(classDf))
            TE = classEffect-baselineEffect
            print c, TE
            #tmpAr = c.split("_")
            tmpAr = getLabel(c);
            fout.write(tmpAr[0]+","+tmpAr[1]+","+tmpAr[2]+","+tmpAr[0]+"_"+tmpAr[1]+","+str(TE)+",random\n")
            
   
def getLabel(c):
    
    tmpAr = c.split("_")
    res=[]
    res.append(0)
    res.append(0)
    res.append(0)
    for i in range(0,3):
        if tmpAr[i] == '0':
            res[i] = 'Bottom 50%'
        else:
            res[i] = 'Top50%'
    return res            
#runTrees()
fout = open(path+"ciResults.csv","w")
fout.write("t_l,e_l,r_l,t_l+e_l,ATE,type\n")
numberOfInstancesThreshold = 1      
calculateEffect()        
runRandom()
fout.close()
print "done"

