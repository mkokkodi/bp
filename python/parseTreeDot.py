



path = "/Users/mkokkodi/Dropbox/projects/beatport/data/"

fin = open(path+"tree.dot")

vars={}
vars['X[0]']='log(fi)'
vars['X[1]']='log(ri)'
vars['X[2]']='e_i'
fout = open(path+"cleanTree.dot","w")
for line in fin.readlines():
    newLine = line
    if "label" in line:
        tmpAr =line.split("gini")
        newLine = tmpAr[0]+"\"];\n"
        #'fi','r_i','e_i','daysAfterRelease'
        newLine = newLine.replace('X[0]','log(f_i)')
        newLine = newLine.replace('X[1]','log(r_i)')
        newLine = newLine.replace('X[2]','e_i')
        newLine = newLine.replace('X[3]','days')
        newLine.replace("\\n","")
        if 'label=""' in newLine:
            tmpAr = line.split("\\n")
            for t in tmpAr:
                if "samples" in t:
                    newLine = newLine.replace("\"\"","\""+t+"\"")
                    newLine = newLine.replace("samples","n")
                    newLine = newLine.replace("]","color=\"lightsteelblue1\" style=\"filled\"]")
        else:
            newLine = newLine.replace("]","color=\"lightsteelblue3\" style=\"filled\"]")
        
    fout.write(newLine)
fout.close()