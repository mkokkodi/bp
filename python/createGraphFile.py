
from sets import Set
path = "/Users/mkokkodi/Dropbox/projects/beatport/data/"

fin = open(path+"graph.csv")

#artistsSet = Set()
#for line in fin.readlines():
#    if "artistId" not in line:
#        artistsSet.add(line.split(",")[0])
#fin.close()

fout = open(path+"graph.dot","w")
fout.write("digraph simple {\n")
index = 0
labelsDict = {}
artistsDict = {}
fin = open(path+"graph.csv")
nodes = Set()
fout.write("subgraph cluster_A {\n")
fout.write("style=filled;\n")
fout.write("color=white;\n")
      
nodeStr = None
for line in fin.readlines():
    if "artistId" not in line:
        artist = line.split(",")[0]
        label = line.split(",")[1]
        if label not in labelsDict:
            labelsDict[label]=index
            index+=1
        if artist not in artistsDict:
            artistsDict[artist]=index
            index+=1
        if artistsDict[artist] not in nodes:
            nodes.add(artistsDict[artist])
            #fout.write("n"+str(artistsDict[artist])+"[label=\""+str(artistsDict[artist])+"\",style=\"filled\",color=\"azure2\",width=0.1, height=0.1];\n")
            fout.write("n"+str(artistsDict[artist])+"[label=\"\",style=\"filled\",color=\"azure2\",width=0.1, height=0.1];\n")
            if nodeStr is not None:
                nodeStr+="->n"+str(artistsDict[artist])
            else:
                nodeStr="n"+str(artistsDict[artist])

fin.close()     
fout.write("edge [style=invisible,dir=none];\n")
fout.write(nodeStr+";\n")          
  
fout.write("}\n")            
fin = open(path+"graph.csv")   
fout.write("subgraph cluster_B {\n")
fout.write("style=filled;\n")
fout.write("color=white;\n")
nodeStr = None
for line in fin.readlines():
    if "artistId" not in line:  
        artist = line.split(",")[0]
        label = line.split(",")[1]
        
        if labelsDict[label] not in nodes:
            nodes.add(labelsDict[label])
            #fout.write("n"+str(labelsDict[label])+"[label=\""+str(labelsDict[label])+"\",style=\"filled\",color=\"green\",width=0.1, height=0.1];\n")
            fout.write("n"+str(labelsDict[label])+"[label=\"\",style=\"filled\",color=\"green\",width=0.1, height=0.1];\n")
         
            if nodeStr is not None:
                nodeStr+="->n"+str(labelsDict[label])
            else:
                nodeStr="n"+str(labelsDict[label])
fin.close()     
fout.write("edge [style=invisible,dir=none];\n")
fout.write(nodeStr+";\n")          
fout.write("}\n")

fout.write("edge[constraint=false];\n")

fin = open(path+"graph.csv")   
for line in fin.readlines():
    artist = line.split(",")[0]
    label = line.split(",")[1]
     
    if "artistId" not in line:     
        collabs = line.split(",")[2]    
        fout.write("n"+str(artistsDict[artist])+"->n"+str(labelsDict[label])+"[weight="+str(collabs)+",label=\""+str(collabs)+"\"]\n")
fout.write("}")
fout.close()
    