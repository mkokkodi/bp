

import numpy as np
from pandas import DataFrame
import warnings
warnings.filterwarnings("ignore")
path = "/Users/mkokkodi/Dropbox/projects/beatport/data/"


#fout = open(path+"quantiles.csv","w")
#fout.write("variable,quantile,score\n")
newTrainDf = DataFrame.from_csv(path+"labelQuantilesRaw.csv", header=False, sep=',',index_col=False)
#labelId,a_l,r_l,t_l,entropy
quantiles = {}
for column in list(newTrainDf.columns.values):
    if column  is not 'labelId':
        q1 = newTrainDf[column].quantile(.25)
        q2 = newTrainDf[column].quantile(.5)
        q3 = newTrainDf[column].quantile(.75)
        quantiles[column]={}
        quantiles[column][0.25]=q1
        quantiles[column][0.5]=q2
        quantiles[column][0.75]=q3
    
print quantiles

#here is the encoding:
# a_l, r_l, t_l, e_l
# 0,0,0,0 -> 0
# 0,0,0,1 -> 1
# 0,0,0,2 -> 2 
# 0,0,0, 3 -> 3
# etc..
# 3,3,3,3
#or binary :P
#For now only number of artists (or releases) and entropy

newTrainDf = DataFrame.from_csv(path+"trainForTrees.csv", header=False, sep=',',index_col=False)
fout =  open(path+"cleanTraining.csv","w")
#trackId,artistId,labelId,a_l,r_l,t_l,entropy,fi,r_i,e_i,days,genre,type
fout.write("trackId,label,fi,r_i,e_i,daysAfterRelease,genre,type\n")
lines = {}
labelCounter = {}
threshold = 10 #how many instances in a bucket to keep
ck=0
for ind,row in newTrainDf.iterrows():
    i=j=k=l=0
    if row['t_l'] <= quantiles['t_l'][0.25]:
        i = 0
    elif row['t_l'] <= quantiles['t_l'][0.5]:
        i = 1
    elif row['t_l'] <= quantiles['t_l'][0.75]:
        i = 1
    else:
        i=1
    if row['entropy'] <= quantiles['entropy'][0.25]:
        j = 0
    elif row['entropy'] <= quantiles['entropy'][0.5]:
        j=0
    elif row['entropy'] <= quantiles['entropy'][0.75]:
        j=1
    else:
        j=1
    if row['r_l'] <= quantiles['r_l'][0.25]:
        k = 0
    elif row['r_l'] <= quantiles['r_l'][0.5]:
        k = 0
    elif row['r_l'] <= quantiles['r_l'][0.75]:
        k = 1
    else:
        k=1
    if k==0:
        ck+=1    
    #label = i * 8 + j * 4 + k * 2 + l
    #label  = i*2 + j
    #print row['a_l'], quantiles['a_l'], i
    label = str(i)+"_"+str(j)+"_"+str(k)
    #label = str(j)
  
            
    if label not  in labelCounter:
        labelCounter[label]=0
    labelCounter[label]+=1
    lines[str(row['trackId'])+","+str(label)+","+str(row['fi'])+","+str(row['r_i'])+","+str(row['e_i'])+","+str(row['daysAfterRelease'])+","+str(row['genre'])+","+str(row['type'])+"\n"]=label

print labelCounter
for line,label in lines.iteritems():
    if labelCounter[label] > threshold:
        fout.write(line)
print "Number of k=0:",ck
fout.close()
    



    