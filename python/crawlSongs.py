import mechanize
from sets import Set
import datetime
import random
import time
import csv
  
import pymysql
import traceback
import sys


newTrack = False
newArtist = False
newRemixer = False

baseLink = "https://pro.beatport.com"
#path = "/home/ubuntu/data/"
path = "/Users/mkokkodi/Desktop/bigdata/beatport/raw/releases/afterMarch23/"
addedLink = "/tracks?start-date=2015-03-24&end-date=2015-05-11"

"""https://pro.beatport.com/genre/breaks/9,https://pro.beatport.com/genre/house/5,
                https://pro.beatport.com/genre/chill-out/10,https://pro.beatport.com/genre/dj-tools/16,
                https://pro.beatport.com/genre/deep-house/12,
                https://pro.beatport.com/genre/drum-and-bass/1,
                https://pro.beatport.com/genre/dubstep/18,
                https://pro.beatport.com/genre/electro-house/17,
                https://pro.beatport.com/genre/electronica/3,
                https://pro.beatport.com/genre/funk-r-and-b/40,
                https://pro.beatport.com/genre/glitch-hop/49,
                https://pro.beatport.com/genre/hard-dance/8,
                https://pro.beatport.com/genre/hardcore-hard-techno/2,
                https://pro.beatport.com/genre/hip-hop/38,
                https://pro.beatport.com/genre/indie-dance-nu-disco/37,
                https://pro.beatport.com/genre/minimal/14,
                https://pro.beatport.com/genre/pop-rock/39,"""
releasedPages="""https://pro.beatport.com/genre/progressive-house/15,
                https://pro.beatport.com/genre/psy-trance/13,
                https://pro.beatport.com/genre/reggae-dub/41,
                https://pro.beatport.com/genre/tech-house/11,
                https://pro.beatport.com/genre/techno/6,
                https://pro.beatport.com/genre/trance/7""".replace("\n","").split(",")


artists = {}
artistsLinks = {}
remixers = {}
#remixersLink = Set()
title=-1
genre = -1
released = -1
price = -1
trackId = -1
rank = -1
label = -1
labelId = -1
subtitle = -1

failedPages=Set()

def initialize():
    global rank,artists,remixers,price, title, genre,released,price,trackId, rank, label, labelId, subtitle
    
   
    artists = {}
    
    remixers = {}
    #remixersLink = Set()
    title=-1
    genre = -1
    released = -1
    price = -1
    trackId = -1
    
    label = -1
    labelId = -1
    subtitle = -1
    price = -1
    rank = -1

def getBrowser():
    br = mechanize.Browser()
    br.addheaders = [('user-agent', '   Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.2.3) Gecko/20100423 Ubuntu/10.04 (lucid) Firefox/3.6.3'), ('accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')]
    ##Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36


    # Don't handle HTTP-EQUIV headers (HTTP headers embedded in HTML).
    br.set_handle_equiv(False)

    # Ignore robots.txt.  Do not do this without thought and consideration.
    br.set_handle_robots(True)

    # Don't add Referer (sic) header
    br.set_handle_referer(False)

    # Don't handle Refresh redirections
    br.set_handle_refresh(False)
    
    return br
    
    
    
def getHTML(br,url):
    page=br.open(url)
    html=page.read() 
    return html


def crawl(htmlPage,outPage):
    global failedPages
    try:
        print "crawling now "+htmlPage
        br = getBrowser()
        html = getHTML(br, htmlPage)
        fout = open("tmp/"+outPage,"w")
        for line in html:
            fout.write(line)
        print "done"
        fout.close()
        return True
    except:
        print "Crawling failed.."
        #print(traceback.format_exc())   
        #print(sys.exc_info()[0])
        failedPages.add(htmlPage)
        return False


def parseTopPage(fin,collectNextLinks):
    
    global fout,rout,arout,artistSong,remixerSong
    global newTrack,newRemixer,rank,artists,remixers,price, title, genre,released,price,trackId, rank, label,  subtitle
    
     
    newRemixer = False
    newTrack = False
    manyPages = False
    nextLinks = Set()
    totalPages = -1
    
    print "Parsing page ",fin
   
 
    for line in fin.readlines():
       
        if " <span class=\"pag-number-ellipsis\">...</span>" in line and collectNextLinks:
            manyPages = True
        if "class=\"pag-number\"" in line and collectNextLinks and not manyPages:
            nextLink=line.split("class=\"pag-number\"")[0]
            nextLink = nextLink.replace("\"","").replace("<a href=","")
            nextLink = nextLink.lstrip()
            nextLinks.add(nextLink)
        if "class=\"pag-number\"" in line and collectNextLinks and  manyPages:
            nextLink=line.split("class=\"pag-number\">")[1]
            totalPages = nextLink.replace("</a>","")
            
                 
        #print line
        if 'bucket-item track' in line:
            #New Track
            newTrack = True
            newRemixer = False
        if newTrack and ' <span class="buk-track-primary-title">' in line:
            title = line.split(">")[1].split("<")[0]
        if  newTrack and "<div class=\"buy-button track-buy-button\" data-track=\""  in line:
            trackId = line.split("data-track=\"")[1].split("\"")[0]
            price  = line.split("data-price=\"")[1].split("\">")[0].replace("$","")
            #print line
            #print rank
       
        if newTrack and ' <span class="buk-track-remixed">' in line:
            subtitle = line.split(">")[1].split("<")[0]
        if newTrack and '<p class="buk-track-artists">' in line:
            newArtist  = True
        if newTrack and  '<p class="buk-track-remixers">' in line:
            newRemixer  = True
       
        if newTrack and 'data-label=' in line:
            #labelId = line.split(" data-label=")[1].split(">")[0].replace("\"","")
            label = line.split(">")[1].split("<")[0]
            #print line
            
        if newTrack and 'data-artist=' in line and newArtist and not newRemixer:
            djId = line.split("data-artist=")[1].split(">")[0].replace("\"","")
            name = line.split(">")[1].split("<")[0]
            
            artists[name]=djId
            #newArtist = False
            #print line
            #print name
        if newTrack and 'data-artist=' in line and newRemixer:
            djId = line.split("data-artist=")[1].split(">")[0].replace("\"","")
            name = (line.split(">")[1].split("<")[0])
           
            remixers[name]=djId
            #print "----->", line
            
            #print line.split("href=\"")[1].split("\"")[0]
            #newRemixer = False
        if newTrack and 'data-genre=' in line:
            genre = line.split(">")[1].split("<")[0]
        if newTrack and '<p class="buk-track-released"' in line:
            released = line.split(">")[1].split("<")[0]
        if newTrack and '</li>' in line:
            
            newTrack = False
            newArtist = False
            newRemixer = False
            fout.write(str(title)+","+str(subtitle)+","+str(trackId)
                                   +","+str(label)+","+str(genre)+","+
                                   str(released)+"\n")
              
            for artist,artistId in artists.iteritems():
                arout.write(str(artistId)+","+str(artist)+","+str(datetime.datetime.today())+"\n")
                artistSong.write(str(artistId)+","+str(trackId)+"\n")
                
            for remixer, remixerId in remixers.iteritems():
                arout.write(str(remixerId)+","+str(remixer)+","+str(datetime.datetime.today())+"\n")
                remixerSong.write(str(remixerId)+","+str(trackId)+"\n")
                    
                    
                  
            #print str(trackId)+","+str(datetime.datetime.today())+","+str(rank)+","+str(price)+"\n"
            initialize()
            #print len(artists)
  
    return nextLinks,totalPages       
            
    #for link in artistsLinks:
        #print link
 
#releases  = open(path+"releases.csv","w")
#releases.write("artistId,songId\n")
 

    
artistSong  = open(path+"artistSongReleases2.csv","w")
artistSong.write("artistId,songId\n")
remixerSong  = open(path+"remixerSongReleases2.csv","w")
remixerSong.write("artistId,songId\n")

arout = open(path+"artistsReleases2.csv","w")
arout.write("artistId,artist\n")
fout = open(path+"songsReleases2.csv","w")
fout.write("title,subtitle,trackId,label,genre,released\n")

  
   
def crawlProcess(link,collectNextLinks):
    if crawl(link,"releases.html"):
        fin = open("tmp/releases.html")
        resultLinks,totalPages = parseTopPage(fin,collectNextLinks)
        fin.close()
        waitingTime = random.randint(0,2)
        print "Waiting for seconds:",waitingTime
        time.sleep(waitingTime)
        return resultLinks,totalPages
    else:
        return [],-1


  
#releasedPages = """https://pro.beatport.com/genre/chill-out/10""".replace("\n","").split(",")


"""
for link in additionalLinks:
    crawlProcess(link,True)
"""            

for link in releasedPages:
        if "&page" not in link:
            tmpLink = link+addedLink
            print tmpLink
            resultLinks,totalPages = crawlProcess(tmpLink,True)
            print "->>>>>:>>>>>>>>>>>>>>>>>> ",len(resultLinks), totalPages
            if int(totalPages) != -1:
                for i in range(2,int(totalPages)+1):
                    crawlProcess(tmpLink+"&page="+str(i),False)
            else:
                for i in range(2,len(resultLinks)+1):
                    crawlProcess(tmpLink+"&page="+str(i),False) 
print "Pages To Crawl Again:"
for line in failedPages:
    print line     
      
#releases.close()
artistSong.close()
remixerSong.close()
arout.close()
fout.close()
