

from pandas import DataFrame
import math



import numpy as np

import warnings

path = "/Users/mkokkodi/Desktop/bigdata/beatport/data/"

#df = DataFrame.from_csv(path+"labelsEntropyRaw.csv", header=False, sep=',',index_col=False)
df = DataFrame.from_csv(path+"artistsEntropiesRaw.csv", header=False, sep=',',index_col=False)

#trackId,labelId,genre  
#labels =  np.unique(df['labelId'].values)
labels =  np.unique(df['artistId'].values)

#total number of genres = 24
genres = []
fin = open(path+"allGenres.csv")
entropies = {}
for line in fin.readlines():
    if 'genre' not in line:
        genres.append(line.replace("\n","").replace("\"",""))

totalSongs = {}
for label in labels:
    #labelDf = df[df['labelId']==label]
    labelDf = df[df['artistId']==label]
    totalSongs[label] = len(labelDf)
    counter = {}
    denom = 0
    for genre in genres:
        counter[genre]=1
        denom+=1
    
    for ind,row in labelDf.iterrows():
        counter[row['genre']]+=1
        denom +=1
    entropy = 0
    for genre,count in counter.iteritems():
        prob = float(count)/float(denom)
        entropy -= prob * math.log(prob)
    entropies[label]=entropy

#fout = open(path+"labelsEntropies.csv","w")
fout = open(path+"artistsEntropies.csv","w")
fout.write("labelId,entropy,numberOfTracks\n")
for label,entropy in entropies.iteritems():
    fout.write(str(label)+","+str(entropy)+","+str(totalSongs[label])+"\n")
fout.close()

        
        
        
    