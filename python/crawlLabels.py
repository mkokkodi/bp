
from sets import Set
import mechanize
import random
import time

path = "/Users/mkokkodi/Desktop/bigdata/beatport/raw/survival/"


baseLink = "https://pro.beatport.com/label/"

artists = {}
artistsLinks = {}
remixers = {}
#remixersLink = Set()
title=-1
genre = -1
released = -1
price = -1
trackId = -1
rank = -1
subtitle = -1
def getBrowser():
    br = mechanize.Browser()
    br.addheaders = [('user-agent', '   Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.2.3) Gecko/20100423 Ubuntu/10.04 (lucid) Firefox/3.6.3'), ('accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')]
    ##Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36


    # Don't handle HTTP-EQUIV headers (HTTP headers embedded in HTML).
    br.set_handle_equiv(False)

    # Ignore robots.txt.  Do not do this without thought and consideration.
    br.set_handle_robots(True)

    # Don't add Referer (sic) header
    br.set_handle_referer(False)

    # Don't handle Refresh redirections
    br.set_handle_refresh(False)
    
    return br
    
    
    
def getHTML(br,url):
    page=br.open(url)
    html=page.read() 
    return html



def crawl(htmlPage,outPage):
    try:
        print "crawling now "+htmlPage
        br = getBrowser()
        html = getHTML(br, htmlPage)
        fout = open("tmp/"+outPage,"w")
        for line in html:
            fout.write(line)
        print "done"
        fout.close()
        return True
    except:
        print "Crawling failed.."
        return False



def initialize():
    global rank,artists,remixers,price, title, genre,released,price,trackId, rank,  subtitle
    
   
    artists = {}
    
    remixers = {}
    #remixersLink = Set()
    title=-1
    genre = -1
    released = -1
    price = -1
    trackId = -1
    
   
    subtitle = -1
    price = -1
    rank = -1
    
def parseTopPage(fin,labelid,getNextLinks):
    
    
    global rout,fout,newRemixer,rank,artists,remixers,price, title, genre,released,price,trackId, rank,  subtitle
 
    newRemixer = False
    newTrack = False
    manyPages=False
    totalPages=-1
    
    if(getNextLinks):
        nextLinks=Set()
    else:
        nextLinks = None
    for line in fin.readlines():
        
        #print "<a href=\"/"+link+"/releases?page="
        if " <span class=\"pag-number-ellipsis\">...</span>" in line and getNextLinks:
            manyPages = True
        if "class=\"pag-number\"" in line and getNextLinks and  manyPages:
            nextLink=line.split("class=\"pag-number\">")[1]
            totalPages = nextLink.replace("</a>","")
            
  
        if getNextLinks and "class=\"pag-number\">" in line and not manyPages:
            #print line
            nextLinks.add(line.split("class=\"pag-number\">")[1].replace("</a>","").replace("\n",""))
            #print nextLinks
        if 'bucket-item track' in line:
            newTrack = True
            
            #print title
            
        if "buk-track-primary-title" in line:
            title = line.split("buk-track-primary-title\">")[1]
            title = title.replace("</span>","").replace("\n","")
        
        if newTrack and 'buk-track-remixed' in line:
            subtitle = line.split("buk-track-remixed\">")[1]
            subtitle = subtitle.replace("</span>","").replace("\n","")
        if newTrack and '<p class="buk-track-artists">' in line:
            newArtist  = True
        if newTrack and  '<p class="buk-track-remixers">' in line:
            newRemixer  = True
 
        if  newTrack and "<div class=\"buy-button track-buy-button\" data-track=\""  in line:
            trackId = line.split("data-track=\"")[1].split("\"")[0]
            price  = line.split("data-price=\"")[1].split("\">")[0].replace("$","")
       
       
        if newTrack and 'data-artist=' in line and newArtist and not newRemixer:
            djId = line.split("data-artist=")[1].split(">")[0].replace("\"","")
            
            name = line.split(">")[1].split("<")[0]
            #print djId,name
            artists[name]=djId
            #newArtist = False
            #print line
            #print name
        if newTrack and 'data-artist=' in line and newRemixer:
            djId = line.split("data-artist=")[1].split(">")[0].replace("\"","")
            name = (line.split(">")[1].split("<")[0])
            remixers[name]=djId
            #print "----->", line
            
            #print line.split("href=\"")[1].split("\"")[0]
            #newRemixer = False
        if newTrack and 'data-genre=' in line:
            genre = line.split(">")[1].split("<")[0]
        if newTrack and '<p class="buk-track-released"' in line:
            released = line.split(">")[1].split("<")[0]
        if newTrack and '</li>' in line:
            #print "Writing Files..."
            #price  = line.split(">")[1].split("<")[0].replace("$","")
            newTrack = False
            newArtist = False
            newRemixer = False
             
            for artist,artistId in artists.iteritems():
                rout.write(str(trackId) +","+str(artistId)+","+str(labelid)+","+str(genre)+","+str(released)+","+str(price)+"\n")
         
            for remixer, remixerId in remixers.iteritems():
                rout.write(str(trackId) +","+str(remixerId)+","+str(labelid)+","+str(genre)+","+str(released)+","+str(price)+"\n")
                    
            #print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
            initialize() 
             
             
    return nextLinks,totalPages 




#Lables to crawl fix tomorrow.   
rout = open(path+"labelReleases2.csv","w")
rout.write("trackId,artistId,labelId,genre,released,price\n")
  
  

#artistsCrawledRecently = getArtistCrawledsInLastMonth()
labelLink={}
#Virgin,35721
#label,labelId
crawlFlag = False
fin = open("/Users/mkokkodi/Dropbox/projects/beatport/data/labelsSurvival.csv")
for line in fin.readlines():
    if "label," not in line:
        if '33835' in line:
            crawlFlag=True
        if crawlFlag:
            line = line.replace("\"","").replace("\n","")
            line = line.replace(" ","%20")
            tmpAr = line.split(",")
            if tmpAr[0] not in labelLink:
                labelLink[tmpAr[0]]=str(tmpAr[1])
    
#labelLink['Virgin']=str(35721)
#labelLink['anjunabeats'] = str(804)
##GET THE NEXT POAGE

print "Labels to crawl:",len(labelLink)
for label,labelId in labelLink.iteritems():
    nextLinks = None
    #link = link.replace("/","",1).replace("\n","")
    #link = "label/permanent-vacation/3222"
    #if crawl(baseLink+"label/permanent-vacation/3222/releases","labelReleases.html"):
    #fin = open("tmp/labelReleases.html")
    #nextLinks = parseTopPage(fin,labelId,True)
    #print baseLink,label,labelId
    if crawl(baseLink+label+"/"+labelId+"/tracks","labelReleases.html"):
        
        #print "Crawling ",link
        waitingTime = random.randint(1, 2)
        print "Waiting for seconds:",waitingTime
        time.sleep(waitingTime)
        
        fin = open("tmp/labelReleases.html")
        
        nextLinks,totalPages = parseTopPage(fin,labelId,True)
   
        #print nextLinks
        if totalPages != -1:
            total = int(totalPages)+1
            for i in range(2,total):
                    if crawl(baseLink+label+"/"+str(labelId)+"/tracks?page="+str(i),"labelReleases.html"):
                    
                        waitingTime = random.randint(1, 2)
                        print "Waiting for seconds:",waitingTime
                        time.sleep(waitingTime)
            
                        fin = open("tmp/labelReleases.html")
                        parseTopPage(fin,labelId,False)
    
        elif nextLinks is not None:
            
            for link2 in nextLinks:
                #print baseLink,label,"/",str(labelId),"/tracks?page=",str(link2)
                if crawl(baseLink+label+"/"+str(labelId)+"/tracks?page="+str(link2),"labelReleases.html"):
                    
                    waitingTime = random.randint(1, 2)
                    print "Waiting for seconds:",waitingTime
                    time.sleep(waitingTime)
        
                    fin = open("tmp/labelReleases.html")
                    parseTopPage(fin,labelId,False)
        #break
rout.close()
