
import mechanize
import random
import time

path = "/Users/mkokkodi/Desktop/bigdata/beatport/raw/"


topTen = """https://pro.beatport.com/genre/chill-out/10,https://pro.beatport.com/genre/breaks/9,https://pro.beatport.com/genre/dj-tools/16,
https://pro.beatport.com/genre/deep-house/12,https://pro.beatport.com/genre/drum-and-bass/1,https://pro.beatport.com/genre/dubstep/18,
https://pro.beatport.com/genre/electro-house/17,https://pro.beatport.com/genre/electronica/3,https://pro.beatport.com/genre/funk-r-and-b/40,
https://pro.beatport.com/genre/glitch-hop/49,https://pro.beatport.com/genre/hard-dance/8,https://pro.beatport.com/genre/hardcore-hard-techno/2,
https://pro.beatport.com/genre/hip-hop/38,https://pro.beatport.com/genre/house/5,https://pro.beatport.com/genre/indie-dance-nu-disco/37,
https://pro.beatport.com/genre/minimal/14,https://pro.beatport.com/genre/pop-rock/39,https://pro.beatport.com/genre/progressive-house/15,
https://pro.beatport.com/genre/progressive-house/15,https://pro.beatport.com/genre/psy-trance/13,https://pro.beatport.com/genre/reggae-dub/41,
https://pro.beatport.com/genre/tech-house/11,https://pro.beatport.com/genre/techno/6,https://pro.beatport.com/genre/trance/7""".replace("\n","").split(",")



def getBrowser():
    br = mechanize.Browser()
    br.addheaders = [('user-agent', '   Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.2.3) Gecko/20100423 Ubuntu/10.04 (lucid) Firefox/3.6.3'), ('accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')]
    ##Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36


    # Don't handle HTTP-EQUIV headers (HTTP headers embedded in HTML).
    br.set_handle_equiv(False)

    # Ignore robots.txt.  Do not do this without thought and consideration.
    br.set_handle_robots(True)

    # Don't add Referer (sic) header
    br.set_handle_referer(False)

    # Don't handle Refresh redirections
    br.set_handle_refresh(False)
    
    return br
    
    
    
def getHTML(br,url):
    page=br.open(url)
    html=page.read() 
    return html



def crawl(htmlPage,outPage):
    try:
        print "crawling now "+htmlPage
        br = getBrowser()
        html = getHTML(br, htmlPage)
        fout = open("tmp/"+outPage,"w")
        for line in html:
            fout.write(line)
        print "done"
        fout.close()
        return True
    except:
        print "Crawling failed.."
        return False

def parseTopPage(fin):
    
    global rout
    global newTrack,newRemixer,rank,artists,remixers,price, title, genre,released,price,trackId, rank, label,  subtitle
    
    newRemixer = False
    newTrack = False
    
    print "Parsing page ",fin
   
    for line in fin.readlines():
        
        #print line
        if 'bucket-item track' in line:
            #New Track
            newTrack = True
            newRemixer = False
        if newTrack and 'data-label=' in line:
            #labelId = line.split(" data-label=")[1].split(">")[0].replace("\"","")
            label = line.split(">")[1].split("<")[0]
            link = line.split("href=\"")[1].split("\"")[0]
            print link,label
            
    
        if newTrack and '<span class="button-text">$' in line:
            rout.write(str(label)+","+str(link)+"\n")
            label = -1
       
         
            
    #for link in artistsLinks:
        #print link

rout = open(path+"labelLink.csv","w")
rout.write("label,link\n")
  
  

#artistsCrawledRecently = getArtistCrawledsInLastMonth()
for top10link in topTen:
    if crawl(top10link+"/top-100","top100Rank.html"):
        waitingTime = random.randint(1, 5)
        print "Waiting for seconds:",waitingTime
        time.sleep(waitingTime)
        fin = open("tmp/top100Rank.html")
        
        links = parseTopPage(fin)
        #break
    
rout.close()
