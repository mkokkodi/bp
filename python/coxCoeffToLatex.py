


path = "/Users/mkokkodi/Dropbox/projects/beatport/data/"
stopParsing = False
fin = open(path+"coxCoeffs.txt")
startParsing=False
coefs={}
expCoefs={}
sign={}
for line in fin.readlines():
    if "---" in line:
        stopParsing = True
        startParsing = False
    if not stopParsing:
        if 'log(r_l)' in line or startParsing:
            startParsing = True
            tmpAr = line.split()
            coefs[tmpAr[0]]=tmpAr[1]
            expCoefs[tmpAr[0]] = tmpAr[2]
            if "*" in line:
                sign[tmpAr[0]]=tmpAr[-1]
            else:
                sign[tmpAr[0]]=''
            
    if 'Likelihood' in line:
        likelihoodRatio = line.split("=")
        likelihoodRatio = likelihoodRatio[1].split()[0]
        pLikelihood = line.split("=")[-1]
    if 'Wald' in line:
        wald = line.split("=")
        wald = wald[1].split()[0]
        pwald = line.split("=")[-1]
        
print "\\bf Variable & \\bf Coefficient & \\bf $\exp(coeff)$ \\\\ "
print "\midrule"
for var,coef in coefs.iteritems():
    print "$",var,"$"," & %.3f" %float(coef),str(sign[var])," & %.3f" %float(expCoefs[var]),str(sign[var]), "\\\\"
print "\midrule"
print        "Likelihood Ratio & ",likelihoodRatio," & p-value = %.3f" %float(pLikelihood), "\\\\"
print      " Wald test &" ,wald," & p-value = %.3f" %float(pwald), "\\\\"
            
          
    
    