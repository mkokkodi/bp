
path = "/Users/mkokkodi/Dropbox/projects/beatport/data/"


labelsPopularity = {}
fin = open(path+"graph.csv")
#artistId,labelClean,collabs,followers
for line in fin.readlines():
    if "artistId" not in line:
        artist = line.split(",")[0]
        label = line.split(",")[1]
        collabs=int(line.split(",")[2])
        followers=int(line.split(",")[3])
        
        if label not in labelsPopularity:
            labelsPopularity[label]=0
        labelsPopularity[label]+=followers * collabs
            
  
fout = open(path+"labelsPopularity.csv","w")
fout.write("label,popularity\n")
            
for key,value in labelsPopularity.iteritems():
    fout.write(str(key)+","+str(value)+"\n")

fout.close()
fin.close()